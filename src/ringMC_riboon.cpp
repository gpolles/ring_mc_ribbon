//============================================================================
// Name        : ringMC_riboon.cpp
// Author      : Guido Polles
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <map>

#include <boost/random.hpp>

/*

#define SQ(a) (a*a)
#define norm(a) ( sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]) )

float** m2f(int nrows, int ncols){
  float** r = new float*[nrows];
  for (int i = 0; i < nrows; ++i) {
    r[i] = new float[ncols];
  }
  return r;
}

void free_m2f(float** m, int nrows){
  for (int i = 0; i < nrows; ++i) {
    delete [] m[i];
  }
  delete [] m;
}

void rotation_matrix(float* _axis, float angle, float** M){
  float n = norm(_axis);
  float axis[3];
  axis[0] = _axis[0] / n;
  axis[1] = _axis[1] / n;
  axis[2] = _axis[2] / n;
  float sint = sin(angle/2);
  float a = cos(angle/2);
  float b = -axis[0] * sint;
  float c = -axis[1] * sint;
  float d = -axis[2] * sint;

  M[0][0] = a*a+b*b-c*c-d*d;
  M[0][1] = 2*(b*c-a*d);
  M[0][2] = 2*(b*d+a*c);

  M[1][0] = 2*(b*c+a*d);
  M[1][1] = a*a+c*c-b*b-d*d;
  M[1][2] = 2*(c*d-a*b);

  M[2][0] = 2*(b*d-a*c);
  M[2][1] = 2*(c*d+a*b);
  M[2][2] = a*a+d*d-b*b-c*c;
}


void vecsub(float* a, float* b, float* res){
  res[0] = a[0] - b[0];
  res[1] = a[1] - b[1];
  res[2] = a[2] - b[2];
}

void vecadd(float* a, float* b, float* res){
  res[0] = a[0] + b[0];
  res[1] = a[1] + b[1];
  res[2] = a[2] + b[2];
}

void matvec(float** m, float* v, float* res){
  for (int i = 0; i < 3; ++i) {
    res[i] = m[i][0]*v[0] + m[i][1]*v[1] + m[i][2]*v[2];
  }
}

boost::mt19937 rng;
boost::uniform_real<float> rnd(0,1);

float **R = m2f(3,3);
void crankshaft_move(int n, float** pos, float** tmp_pos, int* i1, int* i2){
  // choose pivotal points
  boost::uniform_int<> uf(0, n-1);
  *i1 = uf(rng);
  *i2 = uf(rng);
  while(fabs(*i1 - *i2) < 3) *i2 = uf(rng);

  // choose angle
  boost::uniform_real<float> ur(-M_PI, M_PI);
  float angle = ur(rng);

  // perform rotation
  float axis[3];
  float p[3];
  float rp[3];
  vecsub(pos[*i2],pos[*i1],axis);

  float* p0 = pos[*i1];

  rotation_matrix(axis, angle, R);

  for (int i = (*i1)+1; (i%n) != (*i2); ++i) {
    vecsub(pos[i%n], p0, p); // p = p[i] - p0
    matvec(R, p, rp); // rp = R*p
    vecadd(p0, rp, tmp_pos[i%n]); // tmpp[i] = p0 + rp
  }
}

void initialize_ring(int n, float** pos){
  float dt = 2.0*M_PI/n;
  float r = 2.0/dt;

  for (int i = 0; i < n; ++i) {
    pos[i][0] = r*cos(i*dt);
    pos[i][1] = r*sin(i*dt);
    pos[i][2] = 0;
  }
}

void dump_positions(int n, float** pos){
  printf("%d\ncomment\n",n);
  for (int i = 0; i < n; ++i) {
    printf("%s %f %f %f\n", "C", pos[i][0], pos[i][1], pos[i][2]);
  }
}

int main() {
  const int n = 100;
  const int n_steps = 100;

  int i1, i2;

  float** pos = m2f(n, 3);
  float** tmp_pos = m2f(n, 3);
  float** t = m2f(n, 3);

  initialize_ring(n, pos);
  dump_positions(n, pos);

  for (int t = 0; t < n_steps; ++t) {

    crankshaft_move(n, pos, tmp_pos, &i1, &i2);

    // copy modified
    for (int i = i1+1; (i%n) != i2 ; ++i) {
      pos[i%n] = tmp_pos[i%n];
    }

    dump_positions(n, pos);
  }

  free_m2f(pos, n);
  free_m2f(tmp_pos, n);
  free_m2f(R, 3);
  return 0;
}
*/



//#include <mylib/Vector3d.hpp>
//#include <mylib/matrix.hpp>
//
//typedef mylib::Vector3d<float> vec;
//typedef std::vector<mylib::Vector3d<float> > vecs;

typedef double real_t;

// random number generator
boost::mt19937 rng;
boost::uniform_real<real_t> rnd(0,1);

#include <Eigen/Dense>
#include <Eigen/src/Geometry/AngleAxis.h>

typedef Eigen::Matrix<real_t, 3, 1> vec;
typedef std::vector<vec ,Eigen::aligned_allocator<vec> > vecs;
typedef Eigen::Matrix<real_t, 3, 3> Matrix3f;

using namespace std;

inline real_t SQ(real_t a){ return a*a;}

inline int iprev(int i, int n){ return (i - 1 + n) % n; }
inline int inext(int i, int n){ return (i + 1) % n; }

boost::uniform_real<real_t> rnd_pipi(-M_PI, M_PI);
void crankshaft_move(const vecs& pos, vecs* _tmp_pos, int* _i1, int* _i2, Matrix3f* rot_matrix){
  int& i1 = *_i1;
  int& i2 = *_i2;
  vecs& tmp_pos = *_tmp_pos;
  Matrix3f& R = *rot_matrix;
  int n = pos.size();

  // choose pivotal points
  boost::uniform_int<> rnd_0n(0, n-1);
  i1 = rnd_0n(rng);
  i2 = rnd_0n(rng);
  while(fabs(i1 - i2) <= 1) i2 = rnd_0n(rng);

  // choose angle

  real_t angle = rnd_pipi(rng);

  // perform rotation
  vec axis = pos[i2]-pos[i1];
  axis.normalize();
  vec p0 = pos[i1];
  Eigen::AngleAxis<real_t> rot(angle, axis);
  R = rot.toRotationMatrix();
//  mylib::Matrix3d<real_t> R = mylib::rotation_matrix(axis, angle);
  for (int i = i1 + 1; i % n != i2; ++i) {
    vec p = pos[i % n] - p0;
    tmp_pos[i % n] = p0 + (R*p);
  }
}

void decorate(const vecs& p, vecs* _t){
  vecs& t = *_t;
  int n = p.size();
  for (int i = 1; i <= n; ++i) {
    vec v1 = p[i%n] - p[(i-1)%n];
    vec v2 = p[(i+1)%n] - p[i%n];
    t[i%n] = v1.cross(v2).normalized();
  }
}

vec decorate_bead(const vecs& p, int i){
  int n = p.size();

  vec v1 = p[i] - p[iprev(i, n)];
  vec v2 = p[inext(i, n)] - p[i];
  return v1.cross(v2).normalized();
}


void initialize_ring(vecs* _pos, vecs* _t){
  vecs& pos = *_pos;
  vecs& t = *_t;
  int n = pos.size();

  real_t dt = 2.0*M_PI/n;
  real_t R = 2.0/dt;

  for (int i = 0; i < n; ++i) {
    pos[i] = vec(cos(i*dt), sin(i*dt), 0)*R;
    t[i] = vec(0, 0, 1);
  }
}

void dump_positions(const vecs& pos, const vecs& t){
  int n = pos.size();

  printf("%d\ncomment\n",n*3);
  for (int i = 0; i < n; ++i) {
    printf("%s %f %f %f\n", "C", pos[i][0], pos[i][1], pos[i][2]);
    printf("%s %f %f %f\n", "H", pos[i][0] + t[i][0], pos[i][1] + t[i][1], pos[i][2] + t[i][2]);
    printf("%s %f %f %f\n", "H", pos[i][0] - t[i][0], pos[i][1] - t[i][1], pos[i][2] - t[i][2]);
  }
}


// compute energy on bending points.
real_t compute_delta_E(const vecs& t, const vecs& tmp_t, int i1, int i2, real_t k_tors){
  int n = t.size();


  real_t energy = k_tors*(
      1 - SQ(t[iprev(i1, n)].dot( t[i1] )) +
      1 - SQ(t[i1].dot( t[inext(i1, n)] )) +
      1 - SQ(t[iprev(i2, n)].dot( t[i2] )) +
      1 - SQ(t[i2].dot( t[inext(i2, n)] )));

  real_t new_energy = k_tors*(
      1 - SQ(tmp_t[iprev(i1, n)].dot( tmp_t[i1] )) +
      1 - SQ(tmp_t[i1].dot( tmp_t[inext(i1, n)] )) +
      1 - SQ(tmp_t[iprev(i2, n)].dot( tmp_t[i2] )) +
      1 - SQ(tmp_t[i2].dot( tmp_t[inext(i2, n)] )));

  return new_energy - energy;
}

real_t compute_total_energy(const vecs& t, real_t k_tors){
  int n = t.size();
  real_t E = 0;
  for (int i = 0; i < n; ++i) {
    E += k_tors*(1 - SQ(t[i].dot(t[(i+1)%n])));
  }
  return E;
}

typedef std::map<std::string, real_t> dict;

dict read_param_file(char* fname){
  dict params;
  ifstream inf(fname);
  std::string key;
  real_t val;
  while ( !(inf >> key >> val).fail() ){
    params[key] = val;
  }
  return params;
}

int main() {


  int n = 100;
  int n_steps = 100;
  float k_tors = 1.0;

  dict params = read_param_file("input.in");
  if(params.count("N")) n = params["N"];
  if(params.count("STEPS")) n_steps = params["STEPS"];
  if(params.count("K_TORS")) k_tors = params["K_TORS"];

  int n_accept = 0;
  real_t E = 0;

  vecs pos(n);
  vecs tmp_pos(n);
  vecs tv(n);
  vecs tmp_tv(n);
  Matrix3f R;


  initialize_ring(&pos, &tv);
  dump_positions(pos, tv);
  boost::uniform_real<real_t> rand_01(0, 1);

  for (int t = 0; t < n_steps; ++t) {
    bool accept = false;

    // try a move
    int i1, i2;
    crankshaft_move(pos, &tmp_pos, &i1, &i2, &R);

    // to decorate, we also need to copy positions of beads
    // i1, i1-1 and i2, i2+1
    tmp_pos[i1] = pos[i1];
    tmp_pos[iprev(i1 ,n)] = pos[iprev(i1 ,n)];
    tmp_pos[i2] = pos[i2];
    tmp_pos[inext(i2 ,n)] = pos[inext(i2 ,n)];

    // get decorations
    tmp_tv[iprev(i1, n)] = tv[iprev(i1, n)];
    tmp_tv[i1] = decorate_bead(tmp_pos, i1);
    tmp_tv[inext(i1, n)] = decorate_bead(tmp_pos, inext(i1, n));


    tmp_tv[inext(i2, n)] = tv[inext(i2, n)];
    tmp_tv[i2] = decorate_bead(tmp_pos, i2);
    tmp_tv[iprev(i2, n)] = decorate_bead(tmp_pos, iprev(i2, n));

    // get the energy difference coming from the bending
    real_t dE = compute_delta_E(tv, tmp_tv, i1, i2, k_tors);

    // metropolis step
    if (dE < 0){
      accept = true;
    }else{
      if(rand_01(rng) < exp(-dE)) accept = true;
    }

    if(accept){
      ++n_accept;
      E += dE;
      // update positions and transverse vectors
      tv[i1%n] = tmp_tv[i1%n];
      tv[i2%n] = tmp_tv[i2%n];
      for (int i = i1+1; (i%n) != i2 ; ++i) {
        pos[i%n] = tmp_pos[i%n];
        tv[i%n] = decorate_bead(tmp_pos, i%n);
      }

      dump_positions(pos, tv);

    }

    fprintf(stderr, "step: %5d   energy: %6f  energy(total): %6f  acceptance: %.5f\n",
                  t+1,
                  E, compute_total_energy(tv, k_tors), real_t(n_accept)/(t+1));
  }

	return 0;
}

